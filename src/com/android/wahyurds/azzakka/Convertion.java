package com.android.wahyurds.azzakka;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import org.alacyber.merkurius.*;

public class Convertion extends RoboActivity implements OnClickListener {
	private String   amount, flag3;
	private String   res;
	private String   from, destination;
	private String   nominalConvertHasil, resDbl;
	private String   uerel, matauang, whatshadow,descCheck="usual",checkLength;
	String line = null;
	String [] currency;
	private Animation animShow, animHide;
	
	@InjectView(R.id.btn_convertion) private Button mCountConvertion;
	@InjectView(R.id.btn_reset) private Button mReset;
	@InjectView(R.id.spinner) private Spinner spin;
	@InjectView(R.id.spinner2) private Spinner spin2;
	@InjectView(R.id.nominal_konversi) private EditText mNominalConvert;
	@InjectView(R.id.custom_font_convert) private TextView tHeader;		
	
	public String getDescCheck() {
		return descCheck;
	}

	public void setDescCheck(String descCheck) {
		this.descCheck = descCheck;
	}
	
	String [] currency_indonesian ={":: Pilih Mata Uang ::","Indonesia Rupiah (IDR)","US Dollar (USD)","Singapore Dollar (SGD)","Australian Dollar (AUD)","Saudi Riyal (SAR)"};
	String [] currency_english ={":: Select Currency ::","Indonesia Rupiah (IDR)","US Dollar (USD)","Singapore Dollar (SGD)","Australian Dollar (AUD)","Saudi Riyal (SAR)"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.converting);		
		
		Bundle extras = getIntent().getExtras(); 
		if(extras !=null) flag3 = extras.getString("flagintentcurrency");
		
		if(flag3.equals("English")) currency = currency_english;
		else currency = currency_indonesian;
		
		Typeface font = Typeface.createFromAsset(getAssets(), "DroidSans-Bold.ttf");  
	    tHeader.setTypeface(font);
		
	    mCountConvertion.setOnClickListener(this);
	    mReset.setOnClickListener(this);	    		
						
		ArrayAdapter<String> array_currency = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,currency);
		array_currency.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin.setAdapter(array_currency);
		spin2.setAdapter(array_currency);
		
		spin.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position, long id_currency) {
				if(position == 1){
					from = "idr";
				}else if(position == 2){
					from = "usd";
				}else if(position == 3){
					from = "sgd";
				}else if(position == 4){
					from = "aud";
				}else if(position == 5){
					from = "sar";
				}else{
					from= "Tidak Ditemukan";
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		spin2.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int position_to, long id_currency_to) {
				if(position_to == 1){
					destination = "idr";
				}else if(position_to == 2){
					destination = "usd";
				}else if(position_to == 3){
					destination = "sgd";
				}else if(position_to == 4){
					destination = "aud";
				}else if(position_to == 5){
					destination = "sar";
				}else{
					destination= "Tidak Ditemukan";
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});	
		
		initPopup();
	}
	
	public String getConvertion(){
		if( (from.equals("idr") || from.equals("usd") || from.equals("sgd") || from.equals("aud") || from.equals("sar")) && 
				(destination.equals("idr") || destination.equals("usd") || destination.equals("sgd") || destination.equals("aud") || destination.equals("sar")) ){
								
				uerel = "http://www.google.com/ig/calculator?q="+amount+""+from+"=?"+destination;			
				
				String delimeter ="\"";
				String delimeter2 =" ";
				String delimeter3 ="\\.";
				
				URL uploadFileUrl = null; 
		        try {
		            uploadFileUrl = new URL(uerel);
		        } 
		        catch (MalformedURLException e) {
		        	e.printStackTrace();
		        	Toast.makeText(Convertion.this, "Kesalahan Koneksi Internet", Toast.LENGTH_SHORT).show();
		        }
		        try{
		        	BufferedReader urlConnection = new BufferedReader(new InputStreamReader(uploadFileUrl.openStream()));
		            res = urlConnection.readLine();		
		            		          
		            String[] temp  =res.split(delimeter);
					String[] temp2 =temp[3].split(delimeter2);
					String[] temp3 =temp2[0].split(delimeter3);					
					String result1 =temp3[0];
					String result2 =String.valueOf(Math.round(GeneralHelper.convertStringToDouble(temp3[1])));
					
					Pattern pattern = Pattern.compile("[\\D]+");
					Matcher matcher = pattern.matcher(result1);
					//String[] what = pattern.split(result1);
					String matcher2= matcher.replaceAll("");
					
					if(result1.length()<=3){
						checkLength="less";
						if(("million").equals(temp2[1])){
							whatshadow = result1+","+result2;								
							setDescCheck("Million");
						}else if(("billion").equals(temp2[1])){
							whatshadow = result1+","+result2;							
							setDescCheck("Billion");					
						}else if(("trillion").equals(temp2[1])){
							whatshadow = result1+","+result2;							
							setDescCheck("Trillion");					
						}else {
							whatshadow = result1+","+result2;						
						}
					}else{
						checkLength="more";
						if(("million").equals(temp2[1])){
							whatshadow = matcher2;							
							setDescCheck("Million");
						}else if(("billion").equals(temp2[1])){
							whatshadow = matcher2;
							setDescCheck("Billion");
						}else if(("trillion").equals(temp2[1])){
							whatshadow = matcher2;
							setDescCheck("Trillion");
						}else {
							whatshadow = matcher2;					
						}
					}
					
					if(checkLength=="less"){
						resDbl = whatshadow;
					}else{
						resDbl = GeneralHelper.getStrDoubleWithScale2(whatshadow);
					}												         				
		            urlConnection.close();		
				}catch (IOException ioe) {
					ioe.printStackTrace();
					Toast.makeText(Convertion.this, getText(R.string.alert_errorconnection), Toast.LENGTH_SHORT).show();
				}
		}
		return resDbl;
	}
	
	public void onClick(View v) {
		if (v == findViewById(R.id.btn_convertion)) {							
			nominalConvertHasil = mNominalConvert.getText().toString();
			amount = nominalConvertHasil;
			getConvertion();						
			
			if(GeneralHelper.isNullorEmptyString(nominalConvertHasil) && !destination.equals("Tidak Ditemukan") && !from.equals("Tidak Ditemukan")){
				Toast.makeText(Convertion.this, getText(R.string.alert_notfillnominalconvertion), Toast.LENGTH_SHORT).show();
			}else if(!GeneralHelper.isNullorEmptyString(nominalConvertHasil) && destination.equals("Tidak Ditemukan") && !from.equals("Tidak Ditemukan")){
				Toast.makeText(Convertion.this, getText(R.string.alert_destinationconvertion), Toast.LENGTH_SHORT).show();
			}else if(!GeneralHelper.isNullorEmptyString(nominalConvertHasil) && !destination.equals("Tidak Ditemukan") && from.equals("Tidak Ditemukan")){
				Toast.makeText(Convertion.this, getText(R.string.alert_firstvalueconvertion), Toast.LENGTH_SHORT).show();
			}else if(!GeneralHelper.isNullorEmptyString(nominalConvertHasil) && !destination.equals("Tidak Ditemukan") && !from.equals("Tidak Ditemukan")){				
				if(destination == "idr" ){	
					matauang = "Rp. ";
					if(!GeneralHelper.isNullorEmptyString(resDbl)){
						alert();																						
					}else Toast.makeText(Convertion.this, getText(R.string.alert_lostparameter), Toast.LENGTH_SHORT).show(); 						
				}else if(destination == "usd" ){
					matauang = "$. ";
					if(!GeneralHelper.isNullorEmptyString(resDbl)){
						alert();																						
					}else Toast.makeText(Convertion.this, getText(R.string.alert_lostparameter), Toast.LENGTH_SHORT).show();
				}else if(destination == "sgd" ){
					matauang = "$. ";
					if(!GeneralHelper.isNullorEmptyString(resDbl)){
						alert();																						
					}else Toast.makeText(Convertion.this, getText(R.string.alert_lostparameter), Toast.LENGTH_SHORT).show();
				}else if(destination == "aud" ){
					matauang = "$. ";
					if(!GeneralHelper.isNullorEmptyString(resDbl)){
						alert();																						
					}else Toast.makeText(Convertion.this, getText(R.string.alert_lostparameter), Toast.LENGTH_SHORT).show();
				}else if(destination == "sar" ){
					matauang = "Riyal. ";
					if(!GeneralHelper.isNullorEmptyString(resDbl)){
						alert();																						
					}else Toast.makeText(Convertion.this, getText(R.string.alert_lostparameter), Toast.LENGTH_SHORT).show();
				}								
			}else{
				Toast.makeText(Convertion.this, getText(R.string.alert_datanotcompleted), Toast.LENGTH_SHORT).show();
			}
		}
		if (v == findViewById(R.id.btn_reset)) {
			mNominalConvert.setText("");
		}
	}
	
	public void alert(){
		CharSequence nominalDec = resDbl;
		AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
		alertbox.setTitle(getText(R.string.common_resultcalculation));
		if(descCheck=="Million"){
			alertbox.setMessage(getText(R.string.alert_resultconvertion) + matauang+""+nominalDec+" "+ getText(R.string.alert_million));
		}else if(descCheck=="Billion"){
			alertbox.setMessage(getText(R.string.alert_resultconvertion) + matauang+""+nominalDec+" "+ getText(R.string.alert_billion));
		}else if(descCheck=="Trillion"){
			alertbox.setMessage(getText(R.string.alert_resultconvertion) + matauang+""+nominalDec+" "+ getText(R.string.alert_trillion));
		}else{
			alertbox.setMessage(getText(R.string.alert_resultconvertion) + matauang+""+nominalDec);
		}
		
		alertbox.setNeutralButton(getText(R.string.common_ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						arg0.dismiss();
					}
				});
		alertbox.show();
		setDescCheck("");
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}	
	
	private void initPopup() {
    	final TransparentPanel popup = (TransparentPanel) findViewById(R.id.popup_window);
    	String about_indonesian = "Tentang Kami";
    	String about_desc_indonesian = "Kamu dapat menemukan kami di www.alamanahcyber.wordpress.com dan Facebook : Bulletin Jumat Al Amanah, ikuti kami lewat twitter @alamanahcyber. Terimakasih";
    	String about_english = "About Us";
    	String about_desc_english = "Yau can find us on www.alamanahcyber.wordpress.com and Facebook : Bulletin Jumat Al Amanah, follow our twitter @alamanahcyber. Thank You";
    	
    	String button_show_indonesian="Buka";
    	String button_show_english="Open";
    	String button_hide_indonesian="Tutup";
    	String button_hide_english="Close";
    	
    	String about="";
    	String about_desc="";
    	String buttonshow="";
    	String buttonhide="";
    	
    	if(flag3.equals("English")) {
    		about = about_english;
    		about_desc = about_desc_english;
    		buttonshow=button_show_english;
    		buttonhide=button_hide_english;
    	}
		else{
			about = about_indonesian;
    		about_desc = about_desc_indonesian;
    		buttonshow=button_show_indonesian;
    		buttonhide=button_hide_indonesian;
		}
    	
    	popup.setVisibility(View.GONE);
    	animShow = AnimationUtils.loadAnimation( this, R.anim.popup_show);
    	animHide = AnimationUtils.loadAnimation( this, R.anim.popup_hide);
    	
    	final Button   showButton = (Button) findViewById(R.id.show_popup_button);
    	final Button   hideButton = (Button) findViewById(R.id.hide_popup_button);
    	showButton.setText(buttonshow);
    	hideButton.setText(buttonhide);
    	showButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				popup.setVisibility(View.VISIBLE);
				popup.startAnimation( animShow );
				showButton.setEnabled(false);
				hideButton.setEnabled(true);
        }});
        
        hideButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				popup.startAnimation( animHide );
				showButton.setEnabled(true);
				hideButton.setEnabled(false);
				popup.setVisibility(View.GONE);
        }});


    	final TextView locationName = (TextView) findViewById(R.id.location_name);
        final TextView locationDescription = (TextView) findViewById(R.id.location_description);        
        locationName.setText(about);
        locationDescription.setText(about_desc);
	}
}
package com.android.wahyurds.azzakka;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
//import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import org.alacyber.merkurius.*;

public class ZakatPekerjaan extends RoboActivity implements OnClickListener{
	
	@InjectView(R.id.nominal) private EditText mNominal;
	@InjectView(R.id.nominal_pekerjaan) private EditText mNominalNisab;
	@InjectView(R.id.btn_countpekerjaan) private Button mCountZPekerjaan;
	@InjectView(R.id.btn_reset) private Button mReset;	
	@InjectView(R.id.custom_font_zakprof) private TextView tHeader;	
	
	private String   nominal,nominalNisab, flag3;		
	private double   zakatPekerjaanTemp, zakatNisabPekerjaanTemp;
	private Animation animShow, animHide;
	
	private final static double percent=0.025;
	private final static double gram=85;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.zakatpekerjaan);				
		
		Bundle extras = getIntent().getExtras(); 
		if(extras !=null) flag3 = extras.getString("flagintentpopup");
		
		Typeface font = Typeface.createFromAsset(getAssets(), "DroidSans-Bold.ttf");  
	    tHeader.setTypeface(font); 
		
		mCountZPekerjaan.setOnClickListener(this);
		mReset.setOnClickListener(this);
		
		initPopup();
	}
	
	protected void CountZakatPekerjaan(){
		nominal = mNominal.getText().toString();
	}
	
	protected void CountNisabZakatPekerjaan(){
		nominalNisab  = mNominalNisab.getText().toString();
	}
	
	public void onClick(View v) {
		if (v == findViewById(R.id.btn_countpekerjaan)) {
			CountZakatPekerjaan();
			CountNisabZakatPekerjaan();
			
			double nominalDbl = GeneralHelper.convertStringToDouble(nominal);
			zakatPekerjaanTemp = percent * nominalDbl;  
			
			double nominalNisabDbl = GeneralHelper.convertStringToDouble(nominalNisab);
			zakatNisabPekerjaanTemp = gram * nominalNisabDbl;  
		
			if(GeneralHelper.isNullorEmptyString(nominal) && GeneralHelper.isNullorEmptyString(nominalNisab)){				
				Toast.makeText(ZakatPekerjaan.this, getText(R.string.alert_fillall), Toast.LENGTH_SHORT).show();
			}else if(GeneralHelper.isNullorEmptyString(nominal)){				
				Toast.makeText(ZakatPekerjaan.this, getText(R.string.zp_alert1), Toast.LENGTH_SHORT).show();
			}else if(nominal.equals("0")){
				Toast.makeText(ZakatPekerjaan.this, getText(R.string.common_zero), Toast.LENGTH_SHORT).show();
			}else{
				if(GeneralHelper.isNullorEmptyString(nominalNisab)){
					Toast.makeText(ZakatPekerjaan.this, getText(R.string.zp_alert2), Toast.LENGTH_SHORT).show();
				}else if(nominalNisab.equals("0")){
					Toast.makeText(ZakatPekerjaan.this, getText(R.string.common_zero), Toast.LENGTH_SHORT).show();
				}else{
					CharSequence nominalDec = GeneralHelper.getStrDoubleWithScale2(zakatPekerjaanTemp);
					CharSequence nominalNisabDec = GeneralHelper.getStrDoubleWithScale2(zakatNisabPekerjaanTemp);					
					
					if(nominalDbl<zakatNisabPekerjaanTemp){
						AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
						alertbox.setTitle(getText(R.string.common_resultcalculation));
						alertbox.setMessage(getText(R.string.zp_alert3)+" "+nominalDec+
								getText(R.string.zp_alert4)+" "+nominalNisabDec+
								getText(R.string.zp_alert5)+
								getText(R.string.zp_alert6));
						alertbox.setNeutralButton(getText(R.string.common_ok),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface arg0, int arg1) {
										arg0.dismiss();
									}
								});
						alertbox.show();											
					}else{
						AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
						alertbox.setTitle(getText(R.string.common_resultcalculation));
						alertbox.setMessage(getText(R.string.zp_alert3)+" "+nominalDec+
								getText(R.string.zp_alert4)+" "+nominalNisabDec+
								getText(R.string.zp_alert7)+
								getText(R.string.zp_alert8));
						alertbox.setNeutralButton(getText(R.string.common_ok),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface arg0, int arg1) {
										arg0.dismiss();
									}
								});
						alertbox.show();						
					}										
				}
			}
		}
		if (v == findViewById(R.id.btn_reset)) {
			mNominal.setText("");
			mNominalNisab.setText("");			
		}
	}
	
	private void initPopup() {
    	final TransparentPanel popup = (TransparentPanel) findViewById(R.id.popup_window);
    	String about_indonesian = "Tentang Kami";
    	String about_desc_indonesian = "Kamu dapat menemukan kami di www.alamanahcyber.wordpress.com dan Facebook : Bulletin Jumat Al Amanah, ikuti kami lewat twitter @alamanahcyber. Terimakasih";
    	String about_english = "About Us";
    	String about_desc_english = "Yau can find us on www.alamanahcyber.wordpress.com and Facebook : Bulletin Jumat Al Amanah, follow our twitter @alamanahcyber. Thank You";
    	
    	String button_show_indonesian="Buka";
    	String button_show_english="Open";
    	String button_hide_indonesian="Tutup";
    	String button_hide_english="Close";
    	
    	String about="";
    	String about_desc="";
    	String buttonshow="";
    	String buttonhide="";
    	
    	if(flag3.equals("English")) {
    		about = about_english;
    		about_desc = about_desc_english;
    		buttonshow=button_show_english;
    		buttonhide=button_hide_english;
    	}
		else{
			about = about_indonesian;
    		about_desc = about_desc_indonesian;
    		buttonshow=button_show_indonesian;
    		buttonhide=button_hide_indonesian;
		}
    	
    	popup.setVisibility(View.GONE);
    	animShow = AnimationUtils.loadAnimation( this, R.anim.popup_show);
    	animHide = AnimationUtils.loadAnimation( this, R.anim.popup_hide);
    	
    	final Button   showButton = (Button) findViewById(R.id.show_popup_button);
    	final Button   hideButton = (Button) findViewById(R.id.hide_popup_button);
    	showButton.setText(buttonshow);
    	hideButton.setText(buttonhide);
    	showButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				popup.setVisibility(View.VISIBLE);
				popup.startAnimation( animShow );
				showButton.setEnabled(false);
				hideButton.setEnabled(true);
        }});
        
        hideButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				popup.startAnimation( animHide );
				showButton.setEnabled(true);
				hideButton.setEnabled(false);
				popup.setVisibility(View.GONE);
        }});


    	final TextView locationName = (TextView) findViewById(R.id.location_name);
        final TextView locationDescription = (TextView) findViewById(R.id.location_description);        
        locationName.setText(about);
        locationDescription.setText(about_desc);
	}
}

package com.android.wahyurds.azzakka;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import org.alacyber.merkurius.*;

public class Comments extends RoboActivity{

	@InjectView(R.id.id) private EditText txtId;
	@InjectView(R.id.versi) private EditText txtVersi;
	@InjectView(R.id.nama) private EditText txtNama;
	@InjectView(R.id.lokasi) private EditText txtLokasi;	
	@InjectView(R.id.isiSaranAtauKritik) private EditText txtIsi;
	@InjectView(R.id.btnSaveComments) private Button btnSend;
	@InjectView(R.id.btnResetComments) private Button btnReset;
	@InjectView(R.id.custom_font_convert) private TextView titlewhat;
	
	String textId, textVersi, textNama, textLokasi, textIsi, text;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comments);
		
		Typeface font = Typeface.createFromAsset(getAssets(), "DroidSans-Bold.ttf");  
		titlewhat.setTypeface(font);  
		
		txtId.setText("1");
		txtVersi.setText("V1.5");
		
		ButtonReset();
        ButtonSave(); 	
	}
	
	public void ButtonReset(){
		btnReset.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				txtNama.setText("");
				txtLokasi.setText("");
				txtIsi.setText("");				
			}        	
        });
	}
	
	public void ButtonSave(){
		btnSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {				
				textId    = txtId.getText().toString();				
				textVersi  = txtVersi.getText().toString();
				textNama    = txtNama.getText().toString();				
				textLokasi  = txtLokasi.getText().toString();
				textIsi     = txtIsi.getText().toString();
				if(GeneralHelper.isNullorEmptyString(textNama) && GeneralHelper.isNullorEmptyString(textLokasi) && GeneralHelper.isNullorEmptyString(textIsi)){
					Toast.makeText(Comments.this, getText(R.string.alert_fillall), Toast.LENGTH_SHORT).show();
				}else if(GeneralHelper.isNullorEmptyString(textNama) || GeneralHelper.isNullorEmptyString(textLokasi)||GeneralHelper.isNullorEmptyString(textIsi)){
					Toast.makeText(Comments.this, getText(R.string.alert_fillall), Toast.LENGTH_SHORT).show();
				}else{				
					SendData();	
					ButtonReset();
				}
			}        	
        });
	}
	
	public void SendData(){
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://alamanahcyber.16mb.com/application/save.php");  

		try {						
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("id_project", textId));
			nameValuePairs.add(new BasicNameValuePair("version", textVersi));
			nameValuePairs.add(new BasicNameValuePair("name", textNama));
			nameValuePairs.add(new BasicNameValuePair("location", textLokasi));
			nameValuePairs.add(new BasicNameValuePair("content_messages", textIsi));						
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));  			
			
			HttpResponse response = httpclient.execute(httppost);

			InputStream is = response.getEntity().getContent();			
			
			@SuppressWarnings("unused")
			String responseString = generateResponseString(is);			
			
			Toast.makeText(Comments.this, getText(R.string.alert_sent), Toast.LENGTH_SHORT).show();			
			txtNama.setText("");
			txtLokasi.setText("");
			txtIsi.setText("");										
		} catch (ClientProtocolException e) {
			Toast.makeText(Comments.this, getText(R.string.alert_check), Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			Toast.makeText(Comments.this, getText(R.string.alert_check), Toast.LENGTH_SHORT).show();
		}
	}
	
	public String generateResponseString(InputStream stream) {
		InputStreamReader reader = new InputStreamReader(stream);
		BufferedReader buffer = new BufferedReader(reader);
		StringBuilder sb = new StringBuilder();

		try {
			String cur;
			while ((cur = buffer.readLine()) != null) {
				sb.append(cur + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
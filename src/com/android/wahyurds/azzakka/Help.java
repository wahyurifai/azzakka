package com.android.wahyurds.azzakka;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class Help extends Activity{

	private TextView tHeader;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		tHeader = (TextView)findViewById(R.id.custom_font_help);
		
		Typeface font = Typeface.createFromAsset(getAssets(), "DroidSans-Bold.ttf");  
	    tHeader.setTypeface(font);  
	}
}

package com.android.wahyurds.azzakka;


import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SelectLanguage extends ListActivity{
	String[] arrLanguages_english = new String[]{"English", "Indonesian"};
	String[] arrLanguages_indonesian = new String[]{"Bahasa Inggris", "Bahasa Indonesia"};
	String[] arrLanguages;
	String flag4="";	
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle extras = getIntent().getExtras(); 
	    if(extras !=null) flag4 = extras.getString("flagintentlanguage");
	    if(flag4.equals("Indonesian")){
	    	arrLanguages=arrLanguages_indonesian;	    	
		}else{
			arrLanguages=arrLanguages_english;
		}
		setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrLanguages));
	}
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		setResult(RESULT_OK, new Intent().putExtra("language", arrLanguages[position]));
		finish();		
	}
}
package com.android.wahyurds.azzakka;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Counting extends RoboActivity{
	String flag2;
	
	@InjectView(R.id.zakat_fitrah) private Button mZakatFitrah;
	@InjectView(R.id.zakat_maal) private Button mZakatMaal;
	@InjectView(R.id.zakat_pekerjaan) private Button mZakatPekerjaan;
	@InjectView(R.id.konversi) private Button mKonversi;
	@InjectView(R.id.custom_font_count) private TextView tHeader;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.counting);				
		
		Typeface font = Typeface.createFromAsset(getAssets(), "DroidSans-Bold.ttf");  
	    tHeader.setTypeface(font);  
		
	    Bundle extras = getIntent().getExtras(); 
	    if(extras !=null) flag2 = extras.getString("flagintentcurrency");
	    
	    mZakatFitrah.setOnClickListener(new OnClickListener(){
	            public void onClick(View v){
		            	Intent intent = new Intent(Counting.this, ZakatFitrah.class);
		            	intent.putExtra("flagintentpopup", flag2);
		        		startActivity(intent);
	            }
	    });
		
	    mZakatMaal.setOnClickListener(new OnClickListener(){
	            public void onClick(View v){
		            	Intent intent = new Intent(Counting.this, ZakatMaal.class);
		            	intent.putExtra("flagintentpopup", flag2);
		        		startActivity(intent);
	            }
	    });
	     
	    mZakatPekerjaan.setOnClickListener(new OnClickListener(){
	            public void onClick(View v){
		            	Intent intent = new Intent(Counting.this, ZakatPekerjaan.class);
		            	intent.putExtra("flagintentpopup", flag2);
		        		startActivity(intent);
	            }
	    });
	    mKonversi.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
	            	Intent intent = new Intent(Counting.this, Convertion.class);
	            	intent.putExtra("flagintentcurrency", flag2);
	        		startActivity(intent);
            }
	    });
	}
}

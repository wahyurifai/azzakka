package com.android.wahyurds.azzakka;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;


public class Loading extends RoboActivity {	
	@InjectView(R.id.loading) ProgressBar progressBar;
	Handler handler = new Handler();

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loading);		
		onThreadRun();
	}

	public void onThreadRun() {
		new Thread(new Runnable() {
			public void run() {
				
  				try {
					Thread.sleep(1000 * 3);
					startActivity(new Intent().setClass(Loading.this, AzzakkaActivity.class));
					finish();					
				} 
  				catch (Exception e) {}
			}
		}).start();		
	}
}

package com.android.wahyurds.azzakka;

import java.util.Locale;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AzzakkaActivity extends RoboActivity {
	private static final  int REQUEST_CHANGE_LANGUAGE = 1;	
	String flagCurrency="";
	String flagLanguage="";
	
	@InjectView(R.id.zakat) private Button mCount;
	@InjectView(R.id.help) private Button mHelp;
	@InjectView(R.id.information) private Button mInfo;
	@InjectView(R.id.comments) private Button mComments;
	@InjectView(R.id.out) private Button mOut;
	@InjectView(R.id.custom_font_main) private TextView tHeader;
	@InjectView(R.id.bahasa) private Button mLanguage;
	
    @Override
    public void onCreate(Bundle savedInstanceState){    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);       
             
        Typeface font = Typeface.createFromAsset(getAssets(), "DroidSans-Bold.ttf");  
        tHeader.setTypeface(font);
	    
        mHelp.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
	            	Intent intent = new Intent(AzzakkaActivity.this, Help.class);
	        		startActivity(intent);
            }
        });
                
        mInfo.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
	            	Intent intent = new Intent(AzzakkaActivity.this, Information.class);
	        		startActivity(intent);
            }
        });
        
        mCount.setOnClickListener(new OnClickListener(){
            public void onClick(View v) {
            	Configuration c = new Configuration(getResources().getConfiguration());            	
            	
            	if(c.locale.toString().equals("de"))flagCurrency="Indonesian";            		
            	else flagCurrency="English";         		
            	
            	Intent intent = new Intent(AzzakkaActivity.this, Counting.class);
            	intent.putExtra("flagintentcurrency", flagCurrency);
            	intent.putExtra("flagintentpopup", flagCurrency);
            	startActivity(intent);
            }
        });  
        
        mOut.setOnClickListener(new OnClickListener(){
            public void onClick(View v) {
            	Toast.makeText(AzzakkaActivity.this, getText(R.string.common_thank_you), Toast.LENGTH_SHORT).show();
            	AzzakkaActivity.this.finish();            	
            	return;
            }
        });
        
        mComments.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
	            	Intent intent = new Intent(AzzakkaActivity.this, Comments.class);
	        		startActivity(intent);
            }
	    });
        
        mLanguage.setOnClickListener(new OnClickListener(){
            public void onClick(View v){            	
            	Intent intent = new Intent(AzzakkaActivity.this, SelectLanguage.class);   
            	Configuration c = new Configuration(getResources().getConfiguration());            	
            	
            	if(c.locale.toString().equals("de"))flagLanguage="Indonesian";            		
            	else flagLanguage="English";
            	
            	intent.putExtra("flagintentlanguage", flagLanguage);
            	startActivityForResult(intent, REQUEST_CHANGE_LANGUAGE);
            }
	    });
        
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
    	super.onActivityResult(requestCode, resultCode, data);
    	if(resultCode == RESULT_OK && requestCode == REQUEST_CHANGE_LANGUAGE) {
    		Configuration c = new Configuration(getResources().getConfiguration());
    		String language = data.getStringExtra("language");
    		if(language.equals("English") || language.equals("Bahasa Inggris")) {
    			c.locale = Locale.ENGLISH;    			   		
    		}else if(language.equals("Indonesian") || language.equals("Bahasa Indonesia")) {
    			c.locale = Locale.GERMAN;    			
    		}
    		getResources().updateConfiguration(c, getResources().getDisplayMetrics());
    		Intent intent = getIntent();    		
    		overridePendingTransition(0, 0);
    		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    		finish();
    		overridePendingTransition(0, 0);
    		startActivity(intent);    		
    	}
    }

	@Override
	protected void onDestroy() {		
		super.onDestroy();		
	}  
    
}
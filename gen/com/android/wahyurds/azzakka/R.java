/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.android.wahyurds.azzakka;

public final class R {
    public static final class anim {
        public static final int popup_hide=0x7f040000;
        public static final int popup_show=0x7f040001;
    }
    public static final class attr {
    }
    public static final class drawable {
        public static final int azzakka=0x7f020000;
        public static final int black=0x7f020008;
        public static final int blue=0x7f020009;
        public static final int button_bar_gradient=0x7f020001;
        public static final int button_close=0x7f020002;
        public static final int cyan=0x7f02000b;
        public static final int icon=0x7f020003;
        public static final int koin=0x7f020004;
        public static final int orange=0x7f02000a;
        public static final int purple=0x7f02000c;
        public static final int quran=0x7f020005;
        public static final int walpaper=0x7f020006;
        public static final int white=0x7f020007;
        public static final int yellow=0x7f02000d;
    }
    public static final class id {
        public static final int bahasa=0x7f060024;
        public static final int btnChangeLanguage=0x7f06002b;
        public static final int btnResetComments=0x7f060007;
        public static final int btnSaveComments=0x7f060006;
        public static final int btn_convertion=0x7f060010;
        public static final int btn_countfitrah=0x7f060030;
        public static final int btn_countmaal=0x7f060036;
        public static final int btn_countpekerjaan=0x7f06003a;
        public static final int btn_reset=0x7f060011;
        public static final int comments=0x7f060028;
        public static final int count_family=0x7f06002e;
        public static final int custom_font_convert=0x7f060000;
        public static final int custom_font_count=0x7f060018;
        public static final int custom_font_help=0x7f06001e;
        public static final int custom_font_info=0x7f06001f;
        public static final int custom_font_main=0x7f060022;
        public static final int custom_font_zakfit=0x7f06002c;
        public static final int custom_font_zakmaal=0x7f060031;
        public static final int custom_font_zakprof=0x7f060037;
        public static final int description=0x7f060029;
        public static final int description2=0x7f06001d;
        public static final int family_text=0x7f06002d;
        public static final int help=0x7f060026;
        public static final int hide_popup_button=0x7f060015;
        public static final int id=0x7f060001;
        public static final int information=0x7f060025;
        public static final int isiSaranAtauKritik=0x7f060005;
        public static final int konversi=0x7f06001c;
        public static final int layout_top_upload=0x7f060008;
        public static final int loading=0x7f060021;
        public static final int location_description=0x7f060017;
        public static final int location_name=0x7f060016;
        public static final int lokasi=0x7f060004;
        public static final int nama=0x7f060003;
        public static final int nominal=0x7f06002f;
        public static final int nominal_emas=0x7f060035;
        public static final int nominal_emas_text=0x7f060034;
        public static final int nominal_konversi=0x7f06000f;
        public static final int nominal_pekerjaan=0x7f060039;
        public static final int nominal_text=0x7f060033;
        public static final int out=0x7f060027;
        public static final int popup_window=0x7f060014;
        public static final int powered=0x7f060012;
        public static final int show_popup_button=0x7f060013;
        public static final int spinner=0x7f06000b;
        public static final int spinner2=0x7f06000d;
        public static final int text=0x7f06002a;
        public static final int textView1=0x7f060020;
        public static final int title_from=0x7f06000a;
        public static final int title_top_convertion=0x7f060009;
        public static final int title_top_fitrah=0x7f06000c;
        public static final int title_top_maal=0x7f060032;
        public static final int title_top_pekerjaan=0x7f060038;
        public static final int title_top_static=0x7f06000e;
        public static final int versi=0x7f060002;
        public static final int zakat=0x7f060023;
        public static final int zakat_fitrah=0x7f060019;
        public static final int zakat_maal=0x7f06001a;
        public static final int zakat_pekerjaan=0x7f06001b;
    }
    public static final class layout {
        public static final int comments=0x7f030000;
        public static final int converting=0x7f030001;
        public static final int counting=0x7f030002;
        public static final int help=0x7f030003;
        public static final int information=0x7f030004;
        public static final int loading=0x7f030005;
        public static final int main=0x7f030006;
        public static final int what=0x7f030007;
        public static final int zakatfitrah=0x7f030008;
        public static final int zakatmaal=0x7f030009;
        public static final int zakatpekerjaan=0x7f03000a;
    }
    public static final class string {
        public static final int alert_billion=0x7f05004e;
        public static final int alert_check=0x7f050045;
        public static final int alert_datanotcompleted=0x7f05004b;
        public static final int alert_destinationconvertion=0x7f050048;
        public static final int alert_errorconnection=0x7f050046;
        public static final int alert_fillall=0x7f050043;
        public static final int alert_firstvalueconvertion=0x7f050049;
        public static final int alert_lostparameter=0x7f05004a;
        public static final int alert_million=0x7f05004d;
        public static final int alert_notfillnominalconvertion=0x7f050047;
        public static final int alert_resultconvertion=0x7f05004c;
        public static final int alert_sent=0x7f050044;
        public static final int alert_trillion=0x7f05004f;
        public static final int app_name=0x7f050002;
        public static final int az_ac_count_zakah=0x7f050020;
        public static final int az_ac_exit=0x7f050022;
        public static final int az_ac_help=0x7f050021;
        public static final int az_ac_information=0x7f050023;
        public static final int az_ac_select_language=0x7f050025;
        public static final int az_ac_suggest=0x7f050024;
        public static final int cc_example=0x7f050039;
        public static final int cc_from=0x7f050036;
        public static final int cc_nominal=0x7f050038;
        public static final int cc_to=0x7f050037;
        public static final int common_convertion=0x7f05001a;
        public static final int common_count=0x7f050018;
        public static final int common_empty_family=0x7f050017;
        public static final int common_empty_price=0x7f050016;
        public static final int common_erase=0x7f050019;
        public static final int common_help=0x7f05001d;
        public static final int common_ok=0x7f05001c;
        public static final int common_open=0x7f05001f;
        public static final int common_resultcalculation=0x7f05001b;
        public static final int common_thank_you=0x7f050015;
        public static final int common_zero=0x7f05001e;
        public static final int convertion=0x7f050001;
        public static final int counting_currency_convertion=0x7f050029;
        public static final int counting_zakah_fitrah=0x7f050026;
        public static final int counting_zakah_maal=0x7f050027;
        public static final int counting_zakah_profession=0x7f050028;
        public static final int desc=0x7f050003;
        public static final int desc2=0x7f050004;
        public static final int desc3=0x7f050005;
        public static final int descInformation1=0x7f050006;
        public static final int descInformation2=0x7f050007;
        public static final int descInformation3=0x7f050008;
        public static final int descInformation4=0x7f050009;
        public static final int descInformation5=0x7f05000a;
        public static final int descInformation6=0x7f05000b;
        public static final int descInformation7=0x7f05000c;
        public static final int hello=0x7f050000;
        public static final int help=0x7f05000d;
        public static final int help2=0x7f05000e;
        public static final int help3=0x7f05000f;
        public static final int help4=0x7f050010;
        public static final int help5=0x7f050011;
        public static final int help6=0x7f050012;
        public static final int help7=0x7f050013;
        public static final int help8=0x7f050014;
        public static final int powered=0x7f050061;
        public static final int sc_eraseall=0x7f050042;
        public static final int sc_location=0x7f05003c;
        public static final int sc_locationhint=0x7f05003f;
        public static final int sc_name=0x7f05003b;
        public static final int sc_namehint=0x7f05003e;
        public static final int sc_send=0x7f050041;
        public static final int sc_suggest=0x7f05003d;
        public static final int sc_suggesthint=0x7f050040;
        public static final int sc_title=0x7f05003a;
        public static final int zf_alertresult=0x7f050050;
        public static final int zf_example=0x7f05002b;
        public static final int zf_example2=0x7f05002d;
        public static final int zf_nominal=0x7f05002a;
        public static final int zf_nominal_family=0x7f05002c;
        public static final int zm_alert1=0x7f050051;
        public static final int zm_alert2=0x7f050052;
        public static final int zm_alert3=0x7f050053;
        public static final int zm_alert4=0x7f050054;
        public static final int zm_alert5=0x7f050055;
        public static final int zm_alert6=0x7f050056;
        public static final int zm_alert7=0x7f050057;
        public static final int zm_alert8=0x7f050058;
        public static final int zm_example=0x7f050030;
        public static final int zm_example2=0x7f050031;
        public static final int zm_nominal=0x7f05002e;
        public static final int zm_nominal_emas=0x7f05002f;
        public static final int zp_alert1=0x7f050059;
        public static final int zp_alert2=0x7f05005a;
        public static final int zp_alert3=0x7f05005b;
        public static final int zp_alert4=0x7f05005c;
        public static final int zp_alert5=0x7f05005d;
        public static final int zp_alert6=0x7f05005e;
        public static final int zp_alert7=0x7f05005f;
        public static final int zp_alert8=0x7f050060;
        public static final int zp_example=0x7f050034;
        public static final int zp_example2=0x7f050035;
        public static final int zp_nominal=0x7f050032;
        public static final int zp_nominal_emas=0x7f050033;
    }
}
